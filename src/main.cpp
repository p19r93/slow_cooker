// TODO - Re-order function declarations and definitions for clarity

//********** Options ****************//
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-attributes"

#define MIN_TARG_TEMP 15
#define MAX_TARG_TEMP 125  // Thermometer reads up to 125
#define MIN_VALID_TEMPERATURE 0.0  // Somewhat arbitrary
#define MAX_VALID_TEMPERATURE 125.0  // Thermometer reads up to 125
#define REACH_TEMP_TOLERANCE 2.0  // Somewhat arbitrary
#define K_P 0.5  // Proportional
#define K_I 0.0004  // Integral. Units are power change per second-degree of error
#define K_D 60.0 // 'units' are power change per (degree per second) so first guess is 1/3.5E-5
#define MAX_ERROR_FOR_INTEGRAL_UPDATE 3  // degrees
#define MAX_RATE_OF_CHANGE_FOR_INTEGRAL_UPDATE 0.01
#define MIN_PID_UPDATE_INTERVAL_SECONDS 30
#define MIN_TEMP_READ_INTERVAL 1000  // millis
#define SCROLL_UPDATE_INTERVAL 200  // millis
#define SCROLL_INITIAL_WAIT_INTERVAL 2000  // millis
#define SCROLL_END_WAIT_INTERVAL 2000  // millis

#define MIN_PROG_TIME 0
#define MAX_PROG_TIME 600  // minutes
#define MAX_PROG_STEPS 5
#define PROGRAM_NAME_MAX_LENGTH 32

#define POWER_CYCLE_PERIOD 10000  // Period of the PWM cycle for controlling cooker power, in milliseconds
#define MIN_SWITCH_INTERVAL 1000  // Minimum time between on/off switching of the relay, in milliseconds

#define DEFAULT_PROG_END_BUZZER_PERIOD 2000  // Milliseconds, duration of beeping period
#define DEFAULT_PROG_END_BUZZER_PULSE_DURATION 500  // Milliseconds, how long to beep each period
#define DEFAULT_PROG_END_BUZZER_FREQUENCY 3000  // Hz
#define DEFAULT_PROG_END_TOTAL_BUZZING_TIME 0  // Millis. 0 means it never stops
#define DEFAULT_STEP_END_BUZZER_PERIOD 1200  // Milliseconds, duration of beeping period
#define DEFAULT_STEP_END_BUZZER_PULSE_DURATION 400  // Milliseconds, how long to beep each period
#define DEFAULT_STEP_END_BUZZER_FREQUENCY 4500  // Hz
#define DEFAULT_STEP_END_TOTAL_BUZZING_TIME 300000  // Millis. 0 means it never stops
#define DEFAULT_BUZZ_WHEN_PROGRAM_FINISHED true
#define DEFAULT_BUZZ_AT_STEP_END false

#define NUM_LCD_COLS 16  // Strictly, there are 16, but all buffers are to be 16 for the \0

#include <vector>
#include <Arduino.h>
#include <Wire.h>
#include <LiquidCrystal.h>
#include <OneWire.h>
#include <DallasTemperature.h>

typedef void EditActionFunction();
typedef void MenuFunction();
typedef void DisplayFunction();
typedef bool AccessibleCheck();

enum menuCodes: byte{
    MENUCODE_DEFAULT_DISPLAY,
    MENUCODE_MAIN_MENU,
    MENUCODE_MAIN_MENU_CONFIRMATION,
    MENUCODE_SYSTEM_ON_OFF,
    MENUCODE_SYSTEM_ON_OFF_CONFIRMATION,
    MENUCODE_SET_TARG_TEMP,
    MENUCODE_SET_TARG_TYPE,
    MENUCODE_SET_TARG_TIME,
    MENUCODE_SET_BUZZ_AT_STEP_END,
    MENUCODE_CHOOSE_PROGRAM_NEXT_ACTION,
    MENUCODE_PROGRAM_STEP_EDIT_FINISHED,
    MENUCODE_PROGRAM_ENDED,
    NUM_MENUS,
    FIRST_MENUCODE = MENUCODE_DEFAULT_DISPLAY,
    LAST_MENUCODE = MENUCODE_PROGRAM_ENDED,
};

enum mainMenuItemCodes: byte{
    MENUITEMCODE_BACK,
    MENUITEMCODE_PROGRAM,
    MENUITEMCODE_ON_OFF,
    NUM_MENUITEMS,
    FIRST_MENUITEMCODE = MENUITEMCODE_BACK,
    LAST_MENUITEMCODE = MENUITEMCODE_ON_OFF,
};

class menuItem{
  public:
    virtual void Update(){};

    virtual const char* getMenuItemText(){
        return menuItemText;
    }
    const char* setMenuItemText(const char* menuItemText){
        this->menuItemText = menuItemText;
    }

  private:
    const char* menuItemText{};
};

class mainMenuItem: public menuItem {
  public:
    byte nextMenuCodeEnum;

    // Constructor without update function
    mainMenuItem(const char* label, byte nextMenuCode) : menuItem() {
        setMenuItemText(label);
        nextMenuCodeEnum = nextMenuCode;
    }

    // Default constructor required for vector initialisation
    mainMenuItem() : menuItem(){
        setMenuItemText("<NONE>");
        nextMenuCodeEnum = FIRST_MENUCODE;
    }
};


mainMenuItem mainMenuBack = {"Back", MENUCODE_DEFAULT_DISPLAY};
mainMenuItem mainMenuProgram = {"Program", MENUCODE_SET_TARG_TEMP};
mainMenuItem mainMenuOnOff = {"Off", MENUCODE_SYSTEM_ON_OFF};
std::vector<mainMenuItem*> mainMenuItems((size_t)NUM_MENUITEMS);

enum displayCodes: byte{
    DISPLAYCODE_CURR_AND_TARG_TEMPS_ON_OFF,
    DISPLAYCODE_PROG_STEP,
    NUM_DISPLAYS,
    FIRST_DISPLAYCODE = DISPLAYCODE_CURR_AND_TARG_TEMPS_ON_OFF,
    LAST_DISPLAYCODE = DISPLAYCODE_PROG_STEP,
};

enum ActionCodes: byte{
    ACTIONCODE_ADD,
    ACTIONCODE_CLEAR,
    ACTIONCODE_EXE,
    ACTIONCODE_QUIT,
    NUM_PROG_EDIT_ACTIONS,
    FIRST_ACTIONCODE = ACTIONCODE_ADD,
    LAST_ACTIONCODE = ACTIONCODE_QUIT,
};

enum StepBehaviourCodes: byte{
    BEHAVIOURCODE_HOLD,
    BEHAVIOURCODE_HOLD_FOR,
    BEHAVIOURCODE_REACH,
    NUM_BEHAVIOURS,
    FIRST_BEHAVIOURCODE = BEHAVIOURCODE_HOLD,
    LAST_BEHAVIOURCODE = BEHAVIOURCODE_REACH,
};


class ProgramStep {
public:
    int temperature;
    StepBehaviourCodes behaviour;
    unsigned short int timeMinutes;
    bool buzzAtStepEnd;
    bool isEmpty;
    static const byte c_ProgramStepSizeBytes =
              sizeof(temperature)
            + sizeof(behaviour)
            + sizeof(timeMinutes)
            + sizeof(buzzAtStepEnd)
            + sizeof(isEmpty);

    ProgramStep(){
        temperature = 0.0;
        behaviour = FIRST_BEHAVIOURCODE;
        timeMinutes = 0;
        buzzAtStepEnd = DEFAULT_BUZZ_AT_STEP_END;
        isEmpty = true;
    }

    ProgramStep(
            int temperature,
            StepBehaviourCodes behaviour,
            unsigned long timeMinutes=0,
            bool buzzAtStepEnd=DEFAULT_BUZZ_AT_STEP_END,
            bool isEmpty=false){
        this->timeMinutes = timeMinutes;
        this->temperature = temperature;
        this->behaviour = behaviour;
        this->buzzAtStepEnd = buzzAtStepEnd;
        this->isEmpty = isEmpty;
    }
};

class Program {
public:
    char name[PROGRAM_NAME_MAX_LENGTH] = "Uninitialised";  // TODO - do you need this initialisation?
    bool isEmpty = true;
    std::vector<ProgramStep> steps;
    static const byte c_ProgramSizeBytes =
            PROGRAM_NAME_MAX_LENGTH
            + MAX_PROG_STEPS * ProgramStep::c_ProgramStepSizeBytes
            + sizeof(isEmpty);

    Program(){
        static const char newName[] = "<EMPTY>";
        strcpy(name, newName);
        isEmpty = true;
    }

    Program(char name[PROGRAM_NAME_MAX_LENGTH], const std::vector<ProgramStep>& steps){
        strcpy(this->name, name);
        this->steps = steps;  // TODO - Will this work?
    }
};


//********** Buttons ****************//
const int B_SET = A2;  // SET / MENU'
const int B_UP = A1;   // +
const int B_DOWN = A0; // -

const int RELAY = 9;
const int ONEWIRE_BUS = 10; // thermometer
OneWire oneWire(ONEWIRE_BUS);
DallasTemperature sensors(&oneWire);

const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

const int BUZZER = 8;

char lcdRow0Str[NUM_LCD_COLS + 1];
char lcdRow1Str[NUM_LCD_COLS + 1];
std::vector<char*> lcdRowBufs(2);
char oldLcdRow0Str[NUM_LCD_COLS + 1];
char oldLcdRow1Str[NUM_LCD_COLS + 1];
std::vector<char*> oldLcdRowBufs(2);
char tempValStr[6];
char timeValStr[6];

bool placeVisibleCursor = false;
byte visibleCursorCol;
byte visibleCursorRow;
bool oldPlaceVisibleCursor = false;
byte oldVisibleCursorCol;
byte oldVisibleCursorRow;

// const byte NUM_PROG_EDIT_ACTIONS = 3;
char addText[4] = "ADD";
char clearText[4] = "DEL";
char exeText[4] = "EXE";
char quitText[4] = "EXI";
char *progActionTexts[NUM_PROG_EDIT_ACTIONS];

static const char holdText[] = "Hold";
static const char reachHoldText[] = "Reach & Hold";
static const char reachText[] = "Reach";
static const char *behaviourTexts[NUM_BEHAVIOURS];

EditActionFunction *editActionFunctions[NUM_PROG_EDIT_ACTIONS];
MenuFunction *menus[20];  // 20 is totally arbitrary - change it when you wish
DisplayFunction *displays[20];  // 20 is totally arbitrary - change it when you wish

AccessibleCheck *accessibleMenus[NUM_MENUS];
AccessibleCheck *accessibleDisplays[NUM_DISPLAYS];

byte targTemps[MAX_PROG_STEPS];  // Will hold the programmed temperatures
double targetTemperature;
unsigned short int targTimes[MAX_PROG_STEPS];  // Hold the programmed times (minutes to maintain target temp)
byte targTypes[MAX_PROG_STEPS];  // Hold program step behaviour
bool buzzAtStepEnds[MAX_PROG_STEPS];


#define BUTTON_SINGLE_PRESS_DEAD_TIME 100  // millis

class Button {
  private:
    unsigned long timePressed;
    unsigned long lastConsumeTime;
    unsigned long singlePressDeadTime;
    bool freshPress;
    double speedUpFactor = 1.0;
    unsigned long lastSpeedUpTime;

    unsigned long TimeSinceLastConsume(unsigned long time) const{
        return time - lastConsumeTime;
    }

    void ConsumePress(unsigned long time){
        freshPress = false;
        lastConsumeTime = time;
    }

    void SetNewPress(unsigned  long time){
        timePressed = time;
        isPressed = true;
        freshPress = true;
        ResetSpeedUpFactor();
    }

    unsigned long PressedForMillis() const{
        if (isPressed) return millis() - timePressed;
        else return 0;
    }

    unsigned long MillisSinceLastSpeedUp() const{
        if (isPressed){
            return millis() - lastSpeedUpTime;
        } else return 0;
    }

    double GetSpeedUpFactor() const{
        return this->speedUpFactor;
    }

    void SetSpeedUpFactor(double speedUpFactor, double maxSpeedUpFactor){
        if (speedUpFactor > 0.0 && speedUpFactor < maxSpeedUpFactor) {
            this->speedUpFactor = speedUpFactor;
            lastSpeedUpTime = millis();
        }
    }

    void ResetSpeedUpFactor(){ this-> speedUpFactor = 1.0; }

    void SpeedUp(unsigned long minTimeSinceLastSpeedUp, double speedUpJumpFactor, double maxSpeedUpFactor){
        if (MillisSinceLastSpeedUp() > minTimeSinceLastSpeedUp){
            SetSpeedUpFactor(GetSpeedUpFactor() * speedUpJumpFactor, maxSpeedUpFactor);
        }
    }

  public:
    bool isPressed;

    void Press(unsigned long time) {
        if (!isPressed){
            SetNewPress(time);
        }
    }

    void Unpress() {
        isPressed = false;
        speedUpFactor = 1.0;
    }

    bool FreshPress(){
        unsigned long time = millis();
        bool answer = (isPressed && (freshPress || TimeSinceLastConsume(time) >
                (unsigned long)((double) singlePressDeadTime / speedUpFactor)) );
        if (answer) ConsumePress(time);
        return answer;
    }

    bool FreshPress(unsigned long minTimeSinceLastSpeedUp, double speedUpIncreaseFactor, double maxSpeedUpFactor){
        SpeedUp(minTimeSinceLastSpeedUp, speedUpIncreaseFactor, maxSpeedUpFactor);
        return FreshPress();
    }

    Button(){
        isPressed = false;
        freshPress = false;
        timePressed = 0;
        lastConsumeTime = 0;
        singlePressDeadTime = BUTTON_SINGLE_PRESS_DEAD_TIME;
    }

    explicit Button(unsigned long singlePressDeadTime) : Button() {
        this->singlePressDeadTime = singlePressDeadTime;
    }
};

Button downButton(200);
Button upButton(200);
Button setButton(1000);

byte menu = 0;
byte display = 0;
byte currMainMenuSelection = 0;
byte lastTopMainMenuItemShown = 0;
bool freshMenu = true;
double currTemp = 0.0;
unsigned long lastTempReadTime = 0;
double lastTemp = 0.0;
bool temperatureReached = false;
unsigned long holdTemperatureReachedTime = 0;
bool sysOn = false;
unsigned long lastPidUpdateTime = 0;
double errorIntegral = 0.0;
double power = 0.0;
bool relayOn = false;
unsigned long relaySwitchTime = 0;
byte currentProgram = 0;
byte finalProgram = 0;  // Which program ends the current behaviour
byte editingProgram = 0;  // Which program is currently being edited
byte currProgEditAction = 0;
unsigned long lastProgramStepChangeTime = 0;
unsigned long programEndedTime = 0;

bool beeping = false;
bool buzzOn = false;
bool buzzAtProgEnd = DEFAULT_BUZZ_WHEN_PROGRAM_FINISHED;
unsigned long buzzSwitchTime = 0;
unsigned long buzzPeriod;
unsigned long buzzPulseDuration;
unsigned int buzzerFrequency;
unsigned long totalBuzzingTime;
unsigned long buzzingStartTime;

byte bellChar[8] = {
        B00100,
        B01110,
        B01110,
        B01110,
        B11111,
        B00000,
        B00100,
        B00000,
};

byte mutedBellChar[8] = {
        B00100,
        B01110,
        B11111,
        B01110,
        B11111,
        B00000,
        B00100,
        B00000,
};

byte thermometerChar[8] = {
        B00100,
        B01100,
        B00100,
        B01100,
        B00100,
        B01110,
        B01110,
        B00000,
};

byte targetChar[8] = {
        B00000,
        B01110,
        B10101,
        B11111,
        B10101,
        B01110,
        B00000,
        B00000,
};

byte bars[5][8] = {
        { 0x1F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F },  //
        { 0x1F, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x1F },  // |
        { 0x1F, 0x18, 0x18, 0x18, 0x18, 0x18, 0x18, 0x1F },  // ||
        { 0x1F, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1F },  // |||
        { 0x1F, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1E, 0x1F },  // ||||
};

byte endBars[5][8] = {
        { 0x1F, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1F },  //     |
        { 0x1F, 0x11, 0x11, 0x11, 0x11, 0x11, 0x11, 0x1F },  // |   |
        { 0x1F, 0x19, 0x19, 0x19, 0x19, 0x19, 0x19, 0x1F },  // ||  |
        { 0x1F, 0x1D, 0x1D, 0x1D, 0x1D, 0x1D, 0x1D, 0x1F },  // ||| |
        { 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F, 0x1F },  // |||||
};

byte progBarEndChar[8] = {
        B11111,
        B00001,
        B00001,
        B00001,
        B00001,
        B00001,
        B00001,
        B01111,
};

byte progBarEmptyBlockChar[8] = {
        B11111,
        B00000,
        B00000,
        B00000,
        B00000,
        B00000,
        B00000,
        B11111,
};

unsigned int lastDefinedBar;
unsigned long lastProgressBarUpdateTime;
# define PROG_BAR_LENGTH 5
int progressBarBuf[PROG_BAR_LENGTH];
#define MIN_PROGRESS_BAR_UPDATE_INTERVAL 2000  // millis

unsigned short int scrollShift = 0;
unsigned long lastScrollUpdateTime;  // millis
bool scrollEnded = false;
unsigned long scrollEndTime;
#define LEFT_SCROLL_CHAR '<'
#define RIGHT_SCROLL_CHAR '>'

#define BELL_CHAR 1
#define MUTED_BELL_CHAR 2
#define THERMOMETER_CHAR 3
#define TARGET_CHAR 4
#define PROG_BAR_CHAR 5
#define PROG_BAR_END_CHAR 6
#define EMPTY_PROG_BAR_BLOCK_CHAR 7
#define SOLID_PROG_BAR_BLOCK_CHAR 1023
#define DEGREE_CHAR (char)223

bool True();
//bool False();
void LcdProgressBar(int *buf, int length, unsigned long var, unsigned long minVal, unsigned long maxVal);
void ScrollText(char *buf, const char* sourceText, int textLen);
void ResetTextScrolling();
void IncrementMenu();
//byte DecrementMenu();
void IncrementDisplay();
void DecrementDisplay();
void ReadTemp();
void ReadButtons();
void ReadButton(Button* button, int btnPin, unsigned long currentTime);
void DisplayInaccessible();
void MenuInaccessible();
void DisplayCurrTemp(int row);
void DisplayTargTempAndOnOff(int row);
void DefaultDisplayMenu();
void DisplayCurrTargTemp();
void ProgStepDisplay();
void PrintLcd();
void PlaceVisibleCursor(byte col, byte row);
void UnsetVisibleCursor();
void LcdPrintRow(byte row);
unsigned long HoldSecondsRemaining();
bool ProgStepDisplayAccessible();
void SetTargTempMenu();
void SetTargetPower();
void SwitchRelay();
unsigned long PulseDuration();
void TurnRelayOff();
void TurnRelayOn();
void TurnSystemOff();
void TurnSystemOn();
void ToggleSysOn();
void MainMenu();
void BuildLcdRowForMenuItem(char* buf, const char* itemText, bool isSelected, char selectionChar, bool isMore,
                            char isMoreChar);
template <class MenuItemPointerVector>
byte ScrollMenu(MenuItemPointerVector menuOptions, byte firstMenuOption, byte lastMenuOption,
                byte currentSelection, byte lastTopShownItem, char selectionChar = '>');
void DisplayMainMenu();
void MainMenuSelectionMade();
void SystemOnOffMenu();
void SystemOnOffMenuConfirmationMenu();
const char* SystemOnOffText();
void DisplayEditScreen();
void ChooseProgStepBehaviourMenu();
void SetTargTimeMenu();
bool SetTargTimeAccessible();
byte CircularIncrementByte(byte val, byte min, byte max);
byte CircularDecrementByte(byte val, byte min, byte max);
void SetBuzzAtStepEndMenu();
void ChooseProgramNextActionMenu();
bool CanAddAnotherStep();
void ProgramStepEditFinishedMenu();
void AddAction();
void ClearAction();
void ExeAction();
void QuitAction();
void SetNextMenu(byte menuCode);
void SetNextDisplay(byte displayCode);
bool DisplayUpdated();
void StartBuzzing(unsigned int freq, unsigned long totalDuration, unsigned long period, unsigned long pulseDuration);
void StopBuzzing();
void Buzzer();
void BuzzSwitch();
void BuzzOn();
void BuzzOff();
void ExecuteProgram();
bool ReachedTargetTemperature();
void UpdateProgramStep();
void MoveToNextProgramStep();
void ProgramEndedMenu();
bool ComputePid();
void ResetPid();
bool IsValidTemperature(double temperature);


void setup() {
    pinMode(B_SET, INPUT_PULLUP);
    pinMode(B_UP, INPUT_PULLUP);
    pinMode(B_DOWN, INPUT_PULLUP);

    pinMode(RELAY, OUTPUT);
    pinMode(BUZZER, OUTPUT);

    for (int i=0; i<MAX_PROG_STEPS; i++){
        targTemps[i] = MIN_TARG_TEMP;
        targTypes[i] = BEHAVIOURCODE_HOLD;
        targTimes[i] = MIN_PROG_TIME;
        buzzAtStepEnds[i] = false;
    }

    progActionTexts[ACTIONCODE_ADD] = addText;
    progActionTexts[ACTIONCODE_CLEAR] = clearText;
    progActionTexts[ACTIONCODE_EXE] = exeText;
    progActionTexts[ACTIONCODE_QUIT] = quitText;

    editActionFunctions[ACTIONCODE_ADD] = AddAction;
    editActionFunctions[ACTIONCODE_CLEAR] = ClearAction;
    editActionFunctions[ACTIONCODE_EXE] = ExeAction;
    editActionFunctions[ACTIONCODE_QUIT] = QuitAction;

    behaviourTexts[BEHAVIOURCODE_HOLD] = holdText;
    behaviourTexts[BEHAVIOURCODE_HOLD_FOR] = reachHoldText;
    behaviourTexts[BEHAVIOURCODE_REACH] = reachText;

    mainMenuItems[MENUITEMCODE_BACK] = &mainMenuBack;
    mainMenuItems[MENUITEMCODE_PROGRAM] = &mainMenuProgram;
    mainMenuItems[MENUITEMCODE_ON_OFF] = &mainMenuOnOff;

    lcdRowBufs[0] = lcdRow0Str;
    lcdRowBufs[1] = lcdRow1Str;
    oldLcdRowBufs[0] = oldLcdRow0Str;
    oldLcdRowBufs[1] = oldLcdRow1Str;

    displays[DISPLAYCODE_CURR_AND_TARG_TEMPS_ON_OFF] = DisplayCurrTargTemp;
    displays[DISPLAYCODE_PROG_STEP] = ProgStepDisplay;

    accessibleDisplays[DISPLAYCODE_CURR_AND_TARG_TEMPS_ON_OFF] = True;
    accessibleDisplays[DISPLAYCODE_PROG_STEP] = ProgStepDisplayAccessible;

    menus[MENUCODE_DEFAULT_DISPLAY] = DefaultDisplayMenu;
    menus[MENUCODE_MAIN_MENU] = MainMenu;
    menus[MENUCODE_MAIN_MENU_CONFIRMATION] = MainMenuSelectionMade;
    menus[MENUCODE_SYSTEM_ON_OFF] = SystemOnOffMenu;
    menus[MENUCODE_SYSTEM_ON_OFF_CONFIRMATION] = SystemOnOffMenuConfirmationMenu;
    menus[MENUCODE_SET_TARG_TEMP] = SetTargTempMenu;
    menus[MENUCODE_SET_TARG_TYPE] = ChooseProgStepBehaviourMenu;
    menus[MENUCODE_SET_TARG_TIME] = SetTargTimeMenu;
    menus[MENUCODE_SET_BUZZ_AT_STEP_END] = SetBuzzAtStepEndMenu;
    menus[MENUCODE_CHOOSE_PROGRAM_NEXT_ACTION] = ChooseProgramNextActionMenu;
    menus[MENUCODE_PROGRAM_STEP_EDIT_FINISHED] = ProgramStepEditFinishedMenu;
    menus[MENUCODE_PROGRAM_ENDED] = ProgramEndedMenu;

    accessibleMenus[MENUCODE_DEFAULT_DISPLAY] = True;
    accessibleMenus[MENUCODE_MAIN_MENU] = True;
    accessibleMenus[MENUCODE_MAIN_MENU_CONFIRMATION] = True;
    accessibleMenus[MENUCODE_SYSTEM_ON_OFF] = True;
    accessibleMenus[MENUCODE_SYSTEM_ON_OFF_CONFIRMATION] = True;
    accessibleMenus[MENUCODE_SET_TARG_TEMP] = True;
    accessibleMenus[MENUCODE_SET_TARG_TYPE] = True;
    accessibleMenus[MENUCODE_SET_TARG_TIME] = SetTargTimeAccessible;
    accessibleMenus[MENUCODE_SET_BUZZ_AT_STEP_END] = True;
    accessibleMenus[MENUCODE_CHOOSE_PROGRAM_NEXT_ACTION] = True;
    accessibleMenus[MENUCODE_PROGRAM_STEP_EDIT_FINISHED] = True;
    accessibleMenus[MENUCODE_PROGRAM_ENDED] = True;

    // TODO - remove serial comms
    Serial.begin(9600);
    Serial.println(F("Hello, world!"));

    sensors.begin();
    lcd.begin(16, 2);
    lcd.createChar(BELL_CHAR, bellChar);
    lcd.createChar(MUTED_BELL_CHAR, mutedBellChar);
    lcd.createChar(THERMOMETER_CHAR, thermometerChar);
    lcd.createChar(TARGET_CHAR, targetChar);
    lcd.createChar(PROG_BAR_END_CHAR, progBarEndChar);
    lcd.createChar(EMPTY_PROG_BAR_BLOCK_CHAR, progBarEmptyBlockChar);
    lcd.clear();

    // TODO - remove this test
    lcd.print(F("Hello, world!"));
    delay(1000);
}


void loop() {
    // put your main code here, to run repeatedly:

    ReadTemp();  // Read the current temperature
    ReadButtons();

    UnsetVisibleCursor();

    // If SET button is pressed, increment the menu index
    if (setButton.FreshPress()) {
        IncrementMenu();
    }

    menus[menu]();

    if (sysOn){
        SetTargetPower();
        SwitchRelay();
    }

    ExecuteProgram();
    Buzzer();

    if (DisplayUpdated()) {
        PrintLcd();
        memcpy(oldLcdRowBufs[0], lcdRowBufs[0], NUM_LCD_COLS + 1);
        memcpy(oldLcdRowBufs[1], lcdRowBufs[1], NUM_LCD_COLS + 1);
        oldPlaceVisibleCursor = placeVisibleCursor;
        oldVisibleCursorRow = visibleCursorRow;
        oldVisibleCursorCol = visibleCursorCol;
    }

    // delay(10);
}


bool True(){
    return true;
}


//bool False(){
//    return false;
//}


unsigned long MyMap(unsigned long x,
                    unsigned long in_min,
                    unsigned long in_max,
                    unsigned long out_min,
                    unsigned long out_max) {
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}


void LcdProgressBar(int *buf, int length, unsigned long var, unsigned long minVal, unsigned long maxVal){
    unsigned int LINES_PER_BLOCK = 5;
    if (millis() - lastProgressBarUpdateTime > MIN_PROGRESS_BAR_UPDATE_INTERVAL) {
        lastProgressBarUpdateTime = millis();
        unsigned long totalLines = MyMap(var, minVal, maxVal, 0, (unsigned long)LINES_PER_BLOCK * length);
        unsigned int numBlocks = totalLines / LINES_PER_BLOCK;  // Number of whole blocks
        unsigned int lines = totalLines % LINES_PER_BLOCK;  // Leftover lines that need a special char

        for (int x = 0; x < numBlocks; x++)  // 'Print' all the filled blocks
        {
            buf[x] = SOLID_PROG_BAR_BLOCK_CHAR;
        }

        if (numBlocks == length) return;
        else {
            if (numBlocks == 0 && lines == 0){
                lines = 1;
            }

            // If we need to define a special character, and it's different from the last one we defined...
            if (lines != lastDefinedBar && lines != 0) {
                byte *newChar;
                if (numBlocks == length - 1) {
                    newChar = endBars[lines];
                } else {
                    newChar = bars[lines];
                }

                lcd.createChar(PROG_BAR_CHAR, newChar);
                lastDefinedBar = lines;
            }

            // If we're using a special character, print it, otherwise print a blank
            if (lines != 0) {
                buf[numBlocks] = PROG_BAR_CHAR;
            } else if (numBlocks < length - 1) {
                buf[numBlocks] = EMPTY_PROG_BAR_BLOCK_CHAR;
            }

            // Print all the blank blocks, if any
            for (unsigned int x = numBlocks + 1; x < length - 1; x++) {
                buf[x] = EMPTY_PROG_BAR_BLOCK_CHAR;
            }

            // If we haven't already printed the last character
            if (numBlocks != length - 1) {
                buf[length - 1] = PROG_BAR_END_CHAR;
            }

        }
    }
}


void ScrollText(char* buf,  const char* sourceText, int textLen){

    // If we can fit the entire text in, do that
    if (strlen(sourceText) <= textLen){
        for (int i=0; i <= strlen(sourceText); i++){
            buf[i] = sourceText[i];
        }
        buf[strlen(sourceText)] = '\0';
        return;
    } else {
        if (!scrollEnded) {
            // If we're due an update
            if ((scrollShift != 0 && millis() - lastScrollUpdateTime > SCROLL_UPDATE_INTERVAL) ||
                (millis() - lastScrollUpdateTime > SCROLL_INITIAL_WAIT_INTERVAL)) {
                lastScrollUpdateTime = millis();
                scrollShift++;
            }
            // If we've reached the end
            if (scrollShift + textLen > strlen(sourceText)) {
                scrollShift--;  // Freeze here
                scrollEndTime = millis();
                scrollEnded = true;
            }
        }

        if (scrollEnded && (millis() - scrollEndTime > SCROLL_END_WAIT_INTERVAL)){
            ResetTextScrolling();
        }

        if (scrollShift == 0){
            buf[0] = sourceText[0];
        } else {
            buf[0] = LEFT_SCROLL_CHAR;
        }
        for (int i = 1; i < textLen; i++) {
            buf[i] = sourceText[(i + scrollShift) % (strlen(sourceText) - 1)];
        }

        if (!scrollEnded){
            buf[textLen - 1] = RIGHT_SCROLL_CHAR;
        } else {
            buf[textLen - 1] = sourceText[strlen(sourceText) - 1];
        }

        buf[textLen] = '\0';
    }
}


void ResetTextScrolling(){
    scrollShift = 0;
    scrollEnded = false;
    lastScrollUpdateTime = millis();
}


void IncrementMenu(){
    byte nextMenu = CircularIncrementByte(menu, FIRST_MENUCODE, LAST_MENUCODE);
    if (!accessibleMenus[nextMenu]()){
        MenuInaccessible();
        while (!accessibleMenus[nextMenu]()){
            nextMenu = CircularIncrementByte(nextMenu, FIRST_MENUCODE, LAST_MENUCODE);
        }
    }
    SetNextMenu(nextMenu);
}


//byte DecrementMenu(){
//    byte nextMenu = CircularDecrementByte(menu, FIRST_MENUCODE, LAST_MENUCODE);
//    if (!accessibleMenus[nextMenu]()){
//        MenuInaccessible();
//        while (!accessibleMenus[nextMenu]()){
//            nextMenu = CircularDecrementByte(nextMenu, FIRST_MENUCODE, LAST_MENUCODE);
//        }
//    }
//    return nextMenu;
//}


void IncrementDisplay(){
    byte nextDisplay = CircularIncrementByte(display, FIRST_DISPLAYCODE, LAST_DISPLAYCODE);
    if (!accessibleDisplays[nextDisplay]()){
        DisplayInaccessible();
        while (!accessibleDisplays[nextDisplay]()){
            nextDisplay = CircularIncrementByte(nextDisplay, FIRST_DISPLAYCODE, LAST_DISPLAYCODE);
        }
    }
    SetNextDisplay(nextDisplay);
}


void DecrementDisplay(){
    byte nextDisplay = CircularDecrementByte(display, FIRST_DISPLAYCODE, LAST_DISPLAYCODE);
    if (!accessibleDisplays[nextDisplay]()){
        DisplayInaccessible();
        while (!accessibleDisplays[nextDisplay]()){
            nextDisplay = CircularDecrementByte(nextDisplay, FIRST_DISPLAYCODE, LAST_DISPLAYCODE);
        }
    }
    SetNextDisplay(nextDisplay);
}


void ReadTemp(){
    // Read the current temperature and save it into the currTemp variable
    if ((millis() - lastTempReadTime) > MIN_TEMP_READ_INTERVAL) {
        sensors.requestTemperatures();
        double newTemperature = sensors.getTempCByIndex(0);
        if (IsValidTemperature(newTemperature)) {
            lastTemp = currTemp;
            currTemp = newTemperature;
            lastTempReadTime = millis();
//            displayChange = true;
        }
    }
}


void ReadButtons(){
    unsigned long currentTime = millis();
    ReadButton(&downButton, B_DOWN, currentTime);
    ReadButton(&upButton, B_UP, currentTime);
    ReadButton(&setButton, B_SET, currentTime);
};


void ReadButton(Button* button, int btnPin, unsigned long currentTime){
    if (digitalRead(btnPin) == LOW) button->Press(currentTime);
    else button->Unpress();
};


void DisplayInaccessible(){
    sprintf(lcdRowBufs[0], "Not allowed");
    sprintf(lcdRowBufs[1], "");
    PrintLcd();
    delay(1000);
}


void MenuInaccessible(){}  // Does nothing right now - just silently skip past it


void DefaultDisplayMenu(){
    if (upButton.FreshPress()){
        IncrementDisplay();
    }
    if (downButton.FreshPress()){
        DecrementDisplay();
    }
    displays[display]();
}


void DisplayCurrTemp(int row){
    dtostrf(currTemp, 5, 1, tempValStr);
    sprintf(lcdRowBufs[row], "Curr: %s%cC", tempValStr, DEGREE_CHAR);
}


void DisplayTargTempAndOnOff(int row){
    if (sysOn){
        sprintf(lcdRowBufs[row], "Targ: %3d%cC", targTemps[currentProgram], DEGREE_CHAR);
    } else {
        sprintf(lcdRowBufs[row], "System off");
    }
}


void DisplayCurrTargTemp(){
    // The main display, with the current and target temperatures shown
    // string format the current temp (target temp to be displayed as an int)
    freshMenu = false;
    DisplayCurrTemp(0);
    DisplayTargTempAndOnOff(1);
}


void ProgStepDisplay(){
    if (!ProgStepDisplayAccessible()){
        SetNextMenu(MENUCODE_DEFAULT_DISPLAY);
        return;
    }
    // A display, showing the current executing program status
    freshMenu = false;

    dtostrf(currTemp, 4, 1, tempValStr);
    sprintf(lcdRowBufs[0], "%c %s%cC  %c%3d%cC",
            THERMOMETER_CHAR,
            tempValStr,
            DEGREE_CHAR,
            TARGET_CHAR,
            targTemps[currentProgram],
            DEGREE_CHAR);

    if (targTypes[currentProgram] == BEHAVIOURCODE_REACH ||
        (targTypes[currentProgram] == BEHAVIOURCODE_HOLD_FOR && !temperatureReached)
        ){
        sprintf(lcdRowBufs[1], "%1d/%1d Reaching", currentProgram + 1, finalProgram + 1);
    } else if (targTimes[currentProgram] == 0) {
        sprintf(lcdRowBufs[1], "%1d/%1d Holding", currentProgram + 1, finalProgram + 1);
    } else {
        unsigned long totalSecondsRemaining = HoldSecondsRemaining();

        unsigned short hours = totalSecondsRemaining / 3600;
        unsigned short minutes = ((totalSecondsRemaining / 60) + 60) % 60;
        unsigned short seconds = totalSecondsRemaining % 60;

        unsigned short firstVal, secondVal;
        char separator;
        if (hours == 0) {
            firstVal = minutes;
            secondVal = seconds;
            separator = 'm';
        } else {
            firstVal = hours;
            secondVal = minutes;
            separator = 'h';
        }

        LcdProgressBar(progressBarBuf,
                       PROG_BAR_LENGTH,
                       (targTimes[currentProgram] * 60) - totalSecondsRemaining,
                       0,
                       (unsigned long) targTimes[currentProgram] * 60);

        char * pos = lcdRowBufs[1];
        pos += sprintf(lcdRowBufs[1], "%1d/%1d %c %2d%c%02d",
                currentProgram + 1, finalProgram + 1,
                (buzzAtStepEnds[currentProgram]) ? BELL_CHAR : MUTED_BELL_CHAR,
                firstVal, separator, secondVal);
        for (int i : progressBarBuf){
            pos += sprintf(pos, "%c", i);
        }
    }
}


void PrintLcd(){
    LcdPrintRow(0);
    LcdPrintRow(1);
    if (placeVisibleCursor){
        lcd.setCursor(visibleCursorCol, visibleCursorRow);
        lcd.cursor();
    } else {
        lcd.noCursor();
    }
}


void PlaceVisibleCursor(byte col, byte row){
    placeVisibleCursor = true;
    visibleCursorCol = col;
    visibleCursorRow = row;
}

void UnsetVisibleCursor(){
    placeVisibleCursor = false;
}


void LcdPrintRow(byte row){
    lcd.setCursor(0, row);
    lcd.print(lcdRowBufs[row]);
}


unsigned long HoldSecondsRemaining(){
    unsigned long targTimeS = targTimes[currentProgram] * 60;
    unsigned long timePassedS;
    if (targTypes[currentProgram] == BEHAVIOURCODE_HOLD){
        timePassedS = (millis() - lastProgramStepChangeTime) / 1000;
    } else if (targTypes[currentProgram] == BEHAVIOURCODE_HOLD_FOR) {
        timePassedS = (millis() - holdTemperatureReachedTime) / 1000;
    } else {
        return 0;
    }
    if (timePassedS > targTimeS){
        return 0;
    } else {
        return (targTimeS - timePassedS);
    }
}


bool ProgStepDisplayAccessible(){
    return sysOn;
}


void MainMenu(){
    if (freshMenu){
        currMainMenuSelection = FIRST_MENUITEMCODE;
        lastTopMainMenuItemShown = FIRST_MENUITEMCODE;
    }
    freshMenu = false;

    if (upButton.FreshPress()){
        currMainMenuSelection = CircularIncrementByte(currMainMenuSelection, FIRST_MENUITEMCODE, LAST_MENUITEMCODE);
        ResetTextScrolling();
    }

    if (downButton.FreshPress()){
        currMainMenuSelection = CircularDecrementByte(currMainMenuSelection, FIRST_MENUITEMCODE, LAST_MENUITEMCODE);
        ResetTextScrolling();
    }
    DisplayMainMenu();
}


void BuildLcdRowForMenuItem(char* buf, const char* itemText, bool isSelected, char selectionChar, bool isMore,
                            char isMoreChar){
    // Create the string for the top line
    char itemTextBuf[12];
    if (isSelected) {
        ScrollText(itemTextBuf, itemText, 12);
    } else {
        memcpy(itemTextBuf, itemText, 12);
    }
    sprintf(buf, "%c %-12s %c",
            (isSelected) ? selectionChar : ' ',
            itemTextBuf,
            isMore ? isMoreChar : ' ');
}

template <class MenuItemPointerVector>
byte ScrollMenu(MenuItemPointerVector menuOptions, byte firstMenuOption, byte lastMenuOption,
                byte currentSelection, byte lastTopShownItem, char selectionChar){

    // Work out the position the menu should be in
    byte firstShownItem = lastTopShownItem;
    if (lastTopShownItem > currentSelection) {
        // We've moved up beyond the previous displayed options - show the menu starting at the current place
        firstShownItem = currentSelection;
    } else if (currentSelection > lastTopShownItem + 1) {
        // We've moved down beyond previous displayed options - set the top item to be one above the current one
        firstShownItem = currentSelection - 1;
    }

    byte secondShownItem = firstShownItem + 1;

    BuildLcdRowForMenuItem(lcdRowBufs[0],
                           menuOptions[firstShownItem]->getMenuItemText(),
                           (currentSelection == firstShownItem),
                           selectionChar,
                           (firstShownItem > firstMenuOption),
                           '^'
                           );

    BuildLcdRowForMenuItem(lcdRowBufs[1],
                           menuOptions[secondShownItem]->getMenuItemText(),
                           (currentSelection == secondShownItem),
                           selectionChar,
                           (secondShownItem < lastMenuOption),
                           'v'  // Would rather use a better custom character to mirror '^' but that's a pain
                           );

    return firstShownItem;
}


void DisplayMainMenu(){
    for (auto item:mainMenuItems){
        item->Update();
    }
    lastTopMainMenuItemShown = ScrollMenu(mainMenuItems,
                                          FIRST_MENUITEMCODE,
                                          LAST_MENUITEMCODE,
                                          currMainMenuSelection,
                                          lastTopMainMenuItemShown);
}


void MainMenuSelectionMade(){
    freshMenu = false;
     SetNextMenu(mainMenuItems[currMainMenuSelection]->nextMenuCodeEnum);
}


void SystemOnOffMenu(){
    freshMenu = false;
    DisplayMainMenu();
    if (upButton.FreshPress() || downButton.FreshPress()){
        ToggleSysOn();
    }
}


void SystemOnOffMenuConfirmationMenu(){
    freshMenu = false;
    DisplayMainMenu();
    SetNextMenu(MENUCODE_MAIN_MENU);
}


const char* SystemOnOffText(){
    if (sysOn) return "On";
    else return "Off";
}


void DisplayEditScreen() {

    sprintf(lcdRowBufs[0], "%1d TEMP %3d%cC   %c", editingProgram + 1, targTemps[editingProgram], DEGREE_CHAR,
            buzzAtStepEnds[editingProgram]? BELL_CHAR : MUTED_BELL_CHAR);

    if (targTypes[editingProgram] == BEHAVIOURCODE_REACH) {
        sprintf(timeValStr, " --- ");
    } else {
        if (targTimes[editingProgram] > 0) {
            int hours = targTimes[editingProgram] / 60;
            int minutes = targTimes[editingProgram] % 60;
            sprintf(timeValStr, "%2dh%02d", hours, minutes);
        } else sprintf(timeValStr, " INF ");
    }

    char behaviourText[7];
    ScrollText(behaviourText, behaviourTexts[targTypes[editingProgram]], 6);

    sprintf(lcdRowBufs[1], "%6s %5s %3s",
            behaviourText,
            timeValStr,
            progActionTexts[currProgEditAction]);
}


void SetTargTempMenu(){
    // Menu to allow changing the target temperature

    if (upButton.FreshPress(1000, 1.5, 5.0)){
        if (targTemps[editingProgram] < MAX_TARG_TEMP){
            targTemps[editingProgram]++;
//            displayChange = true;
        }
    }

    if (downButton.FreshPress(1000, 1.5, 5.0)){
        if (targTemps[editingProgram] > MIN_TARG_TEMP){
            targTemps[editingProgram]--;
//            displayChange = true;
        }
    }

    DisplayEditScreen();
    PlaceVisibleCursor(9, 0);
}


void ChooseProgStepBehaviourMenu() {
    // Menu to change the step behaviour (e.g. reach, hold)
    if (upButton.FreshPress()){
        targTypes[editingProgram] = CircularIncrementByte(targTypes[editingProgram],
                                                          FIRST_BEHAVIOURCODE,
                                                          LAST_BEHAVIOURCODE);
    }

    if (downButton.FreshPress()){
        targTypes[editingProgram] = CircularDecrementByte(targTypes[editingProgram],
                                                          FIRST_BEHAVIOURCODE,
                                                          LAST_BEHAVIOURCODE);
    }
    DisplayEditScreen();
    // Underline the thing we're editing
    PlaceVisibleCursor(2, 1);
}


void SetTargTimeMenu() {
    // User changes duration of current step to edit
    freshMenu = false;

    if (upButton.FreshPress(1000, 1.5, 20.0)){
        if (targTimes[editingProgram] < MAX_PROG_TIME){
            targTimes[editingProgram] ++;
        }
    }

    if (downButton.FreshPress(1000, 1.5, 20.0)){
        if (targTimes[editingProgram] > MIN_PROG_TIME){
            targTimes[editingProgram] --;
        }
    }

    DisplayEditScreen();
    // Underline the thing we're editing
    PlaceVisibleCursor(11, 1);
}


bool SetTargTimeAccessible(){
    return targTypes[editingProgram] != BEHAVIOURCODE_REACH;
}


byte CircularIncrementByte(byte val, byte min, byte max){
    if (val == max){
        return min;
    } else {
        return val + 1;
    }
}


byte CircularDecrementByte(byte val, byte min, byte max){
    if (val == min){
        return max;
    } else {
        return val - 1;
    }
}


void SetBuzzAtStepEndMenu() {
    // User chooses whether to buzz at the end of the step
    freshMenu = false;

    if (upButton.FreshPress()){
        buzzAtStepEnds[editingProgram] = !buzzAtStepEnds[editingProgram];
    }

    if (downButton.FreshPress()){
        buzzAtStepEnds[editingProgram] = !buzzAtStepEnds[editingProgram];
    }

    DisplayEditScreen();
    // Underline the thing we're editing
    PlaceVisibleCursor(15, 0);
}


void ChooseProgramNextActionMenu() {
    // Will let user choose ADD, EXE, or CLR and take the appropriate action
    freshMenu = false;

    if (upButton.FreshPress()){
        currProgEditAction = CircularIncrementByte(currProgEditAction, FIRST_ACTIONCODE, LAST_ACTIONCODE);
        if (currProgEditAction == ACTIONCODE_ADD && !CanAddAnotherStep()) {
            currProgEditAction = CircularIncrementByte(currProgEditAction, FIRST_ACTIONCODE, LAST_ACTIONCODE);
        }
    }

    if (downButton.FreshPress()){
        currProgEditAction = CircularDecrementByte(currProgEditAction, FIRST_ACTIONCODE, LAST_ACTIONCODE);
        if (currProgEditAction == ACTIONCODE_ADD && !CanAddAnotherStep()) {
            currProgEditAction = CircularDecrementByte(currProgEditAction, FIRST_ACTIONCODE, LAST_ACTIONCODE);
        }
    }

    if (currProgEditAction == ACTIONCODE_ADD && !CanAddAnotherStep()) {
        currProgEditAction = CircularIncrementByte(currProgEditAction, FIRST_ACTIONCODE, LAST_ACTIONCODE);
    }

    DisplayEditScreen();
    PlaceVisibleCursor(13, 1);
}


bool CanAddAnotherStep(){
    // Can we add another step (i.e. is the current-edited step set to run forever)?
    if (targTypes[editingProgram] == BEHAVIOURCODE_REACH || targTimes[editingProgram] != 0){
        return true;
    } else {
        return false;
    }
}


void ProgramStepEditFinishedMenu() {
    // User finished editing the program step so act accordingly
    freshMenu = false;
    editActionFunctions[currProgEditAction]();
}


void AddAction(){
    editingProgram++;
    SetNextMenu(MENUCODE_SET_TARG_TEMP);
};


void ClearAction(){
    if (editingProgram > 0){
        editingProgram--;
    }
    SetNextMenu(MENUCODE_SET_TARG_TEMP);
};


void ExeAction(){
    finalProgram = editingProgram;  // Set the final program so that if you ran EXE from Step 1, it won't run 2+
    buzzAtProgEnd = buzzAtStepEnds[editingProgram];
    editingProgram = 0;
    SetNextMenu(MENUCODE_DEFAULT_DISPLAY);

    SetNextDisplay(DISPLAYCODE_PROG_STEP);

    currentProgram = 0;
    targetTemperature = (double) targTemps[currentProgram];
    lastProgramStepChangeTime = millis();
    TurnSystemOn();
};


void QuitAction(){
    SetNextMenu(MENUCODE_DEFAULT_DISPLAY);
}


void SetTargetPower(){
    ComputePid();
}


void SwitchRelay(){
    // Based on the target power, toggles the relay on or off if required

    unsigned long pulseDur = PulseDuration();
    if (relayOn && pulseDur != POWER_CYCLE_PERIOD && millis() - relaySwitchTime > pulseDur){
        TurnRelayOff();
    } else if (!relayOn && pulseDur != 0 && millis() - relaySwitchTime > POWER_CYCLE_PERIOD - pulseDur){
        TurnRelayOn();
    }
}


unsigned long PulseDuration(){
    unsigned long dur;
    dur = POWER_CYCLE_PERIOD * power;
    if (dur < MIN_SWITCH_INTERVAL){
        dur = 0;
    } else if (dur > POWER_CYCLE_PERIOD - MIN_SWITCH_INTERVAL) {
        dur = POWER_CYCLE_PERIOD;
    }
    return dur;
}


void TurnRelayOff(){
    digitalWrite(RELAY, LOW);
    relayOn = false;
    relaySwitchTime = millis();
}


void TurnRelayOn(){
    digitalWrite(RELAY, HIGH);
    relayOn = true;
    relaySwitchTime = millis();
}


void TurnSystemOff(){
    TurnRelayOff();
    sysOn = false;
    mainMenuOnOff.setMenuItemText(SystemOnOffText());
}


void TurnSystemOn(){
    sysOn = true;
    mainMenuOnOff.setMenuItemText(SystemOnOffText());
}


void ToggleSysOn(){
    if (sysOn) TurnSystemOff();
    else TurnSystemOn();
}


void SetNextMenu(byte menuCode){
    menu = menuCode;
    ResetTextScrolling();
    freshMenu = true;
}


void SetNextDisplay(byte displayCode){
    display = displayCode;
    ResetTextScrolling();
    freshMenu = true;
}


bool DisplayUpdated(){
    bool textChanged = (
            (strcmp(oldLcdRowBufs[0], lcdRowBufs[0]) != 0) ||
            (strcmp(oldLcdRowBufs[1], lcdRowBufs[1]) != 0)
            );
    bool visibleCursorChanged = (
            (placeVisibleCursor != oldPlaceVisibleCursor) ||
            (visibleCursorCol != oldVisibleCursorCol) ||
            (visibleCursorRow != oldVisibleCursorRow)
            );

    return (textChanged || visibleCursorChanged);
}


void StartBuzzing(unsigned int freq, unsigned long totalDuration, unsigned long period, unsigned long pulseDuration){
    totalBuzzingTime = totalDuration;
    buzzerFrequency = freq;
    buzzPeriod = period;
    buzzPulseDuration = pulseDuration;
    buzzingStartTime = millis();
    beeping = true;
}


void StopBuzzing(){
    BuzzOff();
    beeping = false;
}


void Buzzer(){
    if (beeping) {
        if ((setButton.isPressed || upButton.isPressed || downButton.isPressed) ||
                (totalBuzzingTime != 0 && millis() - buzzingStartTime > totalBuzzingTime)){
            StopBuzzing();
        } else {
            BuzzSwitch();
        }
    }
}


void BuzzSwitch(){
    if (buzzOn && millis() - buzzSwitchTime > buzzPulseDuration){
        BuzzOff();
    } else if (!buzzOn && (millis() - buzzSwitchTime > buzzPeriod - buzzPulseDuration)){
        BuzzOn();
    }
}


void BuzzOn(){
    buzzSwitchTime = millis();
    buzzOn = true;
    tone(BUZZER, buzzerFrequency);
}


void BuzzOff(){
    buzzSwitchTime = millis();
    buzzOn = false;
    noTone(BUZZER);
}


void ExecuteProgram(){
    if (sysOn){
        UpdateProgramStep();
    }
}


bool ReachedTargetTemperature(){
    // Have we reached the target temperature?

    // First check whether we're within tolerance of it now
    if (abs(currTemp - targTemps[currentProgram]) < REACH_TEMP_TOLERANCE){
        return true;
    }

    // Just in case, check we didn't way overshoot it since the last temperature check
    if ((lastTemp - targTemps[currentProgram]) * (currTemp - targTemps[currentProgram]) < 0.0){
        return true;
    }

    return false;
}


void UpdateProgramStep(){
    if (targTypes[currentProgram] == BEHAVIOURCODE_REACH) {
        if (ReachedTargetTemperature()) {  // If we've passed the target
            MoveToNextProgramStep();
        }
    } else if (targTypes[currentProgram] == BEHAVIOURCODE_HOLD_FOR) {
        // Have we already reached the temperature, and have now held it long enough to move on?
        if (temperatureReached && millis() - holdTemperatureReachedTime > (targTimes[currentProgram] * 60000)){
            MoveToNextProgramStep();
        }
        // Have we freshly reached the temperature?
        else if (!temperatureReached && ReachedTargetTemperature()) {
            temperatureReached = true;
            holdTemperatureReachedTime = millis();
        }
    } else if (targTypes[currentProgram] == BEHAVIOURCODE_HOLD &&
               targTimes[currentProgram] != 0 &&
               millis() - lastProgramStepChangeTime > (targTimes[currentProgram] * 60000)){
        MoveToNextProgramStep();
    }
}


void MoveToNextProgramStep(){
    lastProgramStepChangeTime = millis();
    ResetPid();
    temperatureReached = false;
    if (currentProgram == finalProgram){
        TurnSystemOff();
        SetNextMenu(MENUCODE_PROGRAM_ENDED);
        if (buzzAtProgEnd) {
            StartBuzzing(DEFAULT_PROG_END_BUZZER_FREQUENCY, DEFAULT_PROG_END_TOTAL_BUZZING_TIME, DEFAULT_PROG_END_BUZZER_PERIOD,
                         DEFAULT_PROG_END_BUZZER_PULSE_DURATION);
        }
    } else {
        if (buzzAtStepEnds[currentProgram]){
            StartBuzzing(DEFAULT_STEP_END_BUZZER_FREQUENCY, DEFAULT_STEP_END_TOTAL_BUZZING_TIME, DEFAULT_STEP_END_BUZZER_PERIOD,
                         DEFAULT_STEP_END_BUZZER_PULSE_DURATION);
        }
        currentProgram++;
        targetTemperature = (double)targTemps[currentProgram];
    }
}


void ProgramEndedMenu(){
    if (freshMenu) programEndedTime = millis();

    unsigned long totalSecondsAgo = (millis() - programEndedTime) / 1000;
    freshMenu = false;

    sprintf(lcdRowBufs[0], "Program ended");

    unsigned short minutesAgo = ((totalSecondsAgo / 60) + 60) % 60;
    unsigned long hoursAgo = (totalSecondsAgo / 60) / 60;
    unsigned short secondsAgo = (totalSecondsAgo + 60) % 60;

    sprintf(lcdRowBufs[1], "%2luh%02dm%02d ago", hoursAgo, minutesAgo, secondsAgo);


    if (setButton.isPressed || downButton.isPressed || upButton.isPressed ){
        SetNextMenu(MENUCODE_DEFAULT_DISPLAY);
    }
}


bool ComputePid(){
    // Returns true if it updated, false otherwise
    double timeStepSecs = ((double)(millis() - lastPidUpdateTime)) / (double)1000;
    if (timeStepSecs > MIN_PID_UPDATE_INTERVAL_SECONDS){
        lastPidUpdateTime = millis();
        double error = currTemp - targetTemperature;
        double rateOfChange = (currTemp - lastTemp) / timeStepSecs;

        if (abs(error) < MAX_ERROR_FOR_INTEGRAL_UPDATE && abs(rateOfChange) < MAX_RATE_OF_CHANGE_FOR_INTEGRAL_UPDATE) {
            errorIntegral += error * timeStepSecs;
        }

        power = -(K_P * error + K_I * errorIntegral + K_D * rateOfChange);

        return true;
    } else {
        return false;
    }
}


void ResetPid(){
    lastPidUpdateTime = millis();
    errorIntegral = 0.0;
}


bool IsValidTemperature(double temperature){
    if (temperature >= MIN_VALID_TEMPERATURE && temperature <= MAX_VALID_TEMPERATURE){
        return true;
    } else {
        return false;
    }
}

#pragma clang diagnostic pop